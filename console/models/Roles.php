<?php

namespace console\models;

use Yii;
use console\models\Rigths;


/**
 * This is the model class for table "roles".
 *
 * @property integer $id
 * @property string $name
 *
 * @property RolesRights[] $rolesRights
 * @property UserRoles[] $userRoles
 */
class Roles extends \yii\db\ActiveRecord
{
    public $roleRights;


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'roles';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 255],
            [['name'], 'unique'],
            // [['rights'], 'skipOnEmpty' => false],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название роли',
            'rights' => 'Права',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRigths()
    {
        return $this->hasMany(Rigths::className(), ['id' => 'right_id'])
                ->viaTable('roles_rights', ['role_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['id' => 'role_id'])
                ->viaTable('users_roles', ['user_id' => 'id']);

    }
}

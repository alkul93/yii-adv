<?php

namespace console\models;

use Yii;
use console\models\Roles;


/**
 * This is the model class for table "rights".
 *
 * @property integer $id
 * @property string $name
 *
 * @property RolesRights[] $rolesRights
 * @property UsersRights[] $usersRights
 */
class Rigths extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'rights';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 255],
            [['name'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название правила',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRoles()
    {
        return $this->hasMany(Roles::className(), ['id' => 'right_id'])
                ->viaTable('roles_rights', ['role_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['id' => 'right_id'])
                ->viaTable('users_rights', ['user_id' => 'id']);
    }
}

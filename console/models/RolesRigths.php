<?php

namespace console\models;

use Yii;
use console\models\Roles;
use console\models\Rigths;

/**
 * This is the model class for table "roles_rights".
 *
 * @property integer $role_id
 * @property integer $right_id
 *
 * @property Rights $right
 * @property Roles $role
 */
class RolesRigths extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'roles_rights';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['role_id', 'right_id'], 'required'],
            [['role_id', 'right_id'], 'integer'],
            [['right_id'], 'exist', 'skipOnError' => true, 'targetClass' => Rigths::className(), 'targetAttribute' => ['right_id' => 'id']],
            [['role_id'], 'exist', 'skipOnError' => true, 'targetClass' => Roles::className(), 'targetAttribute' => ['role_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'role_id' => 'Role ID',
            'right_id' => 'Right ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRight()
    {
        return $this->hasOne(Rigths::className(), ['id' => 'right_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRole()
    {
        return $this->hasOne(Roles::className(), ['id' => 'role_id']);
    }
}

<?php

namespace console\models;

use Yii;
use console\models\Rigths;

/**
 * This is the model class for table "users_rights".
 *
 * @property integer $user_id
 * @property integer $right_id
 *
 * @property Rights $right
 * @property User $user
 */
class UsersRigths extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users_rights';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'right_id'], 'required'],
            [['user_id', 'right_id'], 'integer'],
            [['right_id'], 'exist', 'skipOnError' => true, 'targetClass' => Rigths::className(), 'targetAttribute' => ['right_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_id' => 'User ID',
            'right_id' => 'Right ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRight()
    {
        return $this->hasOne(Rigths::className(), ['id' => 'right_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}

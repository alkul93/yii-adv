<?php

use yii\db\Migration;

class m161005_202207_user_to_rights extends Migration
{
    public function up()
    {
        $this->createTable('users_rights', [
            'user_id' => $this->integer()->notNull(),
            'right_id' => $this->integer()->notNull(),
        ]);

        $this->createIndex('idx-user-user_id', 'users_rights', 'user_id');
        $this->createIndex('idx-user-right_id', 'users_rights', 'right_id');

        $this->addForeignKey('fk-user-user_id', 'users_rights', 'user_id', 'user', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk-user-right_id', 'users_rights', 'right_id', 'rights', 'id', 'CASCADE', 'CASCADE');

    }

    public function down()
    {
        $this->dropForeignKey('fk-user-user_id', 'users_rights');
        $this->dropForeignKey('fk-user-right_id', 'users_rights');
        $this->dropIndex('idx-user-user_id', 'users_rights');
        $this->dropIndex('idx-user-right_id', 'users_rights');
        $this->dropTable('users_rights');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

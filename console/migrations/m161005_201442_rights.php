<?php

use yii\db\Migration;

class m161005_201442_rights extends Migration
{
    public function up()
    {
        $this->createTable('rights', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->unique(),
        ]);

        $this->insert('rights', [
            'name' => 'Редакитрование накладной для приема в СЦ',
        ]);
        $this->insert('rights', [
            'name' => 'Редактирование заказа',
        ]);
        $this->insert('rights', [
            'name' => 'Редактирование пользователей',
        ]);
        $this->insert('rights', [
            'name' => 'Создание заказа',
        ]);
        $this->insert('rights', [
            'name' => 'Создание накладной для приема в СЦ',
        ]);
        $this->insert('rights', [
            'name' => 'Создание пользователей',
        ]);
    }

    public function down()
    {
        $this->dropTable('rights');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

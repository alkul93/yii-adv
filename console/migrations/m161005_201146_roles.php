<?php

use yii\db\Migration;

class m161005_201146_roles extends Migration
{
    public function up()
    {
        $this->createTable('roles', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->unique(),
        ]);

        $this->insert('roles', [
            'name' => 'Сервисный инженер',
        ]);
        $this->insert('roles', [
            'name' => 'Консультант',
        ]);
        $this->insert('roles', [
            'name' => 'Администратор',
        ]);
    }

    public function down()
    {
        $this->dropTable('roles');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

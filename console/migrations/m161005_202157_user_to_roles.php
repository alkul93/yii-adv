<?php

use yii\db\Migration;

class m161005_202157_user_to_roles extends Migration
{
    public function up()
    {
        $this->createTable('users_roles', [
            'user_id' => $this->integer()->notNull(),
            'role_id' => $this->integer()->notNull(),
        ]);

        $this->createIndex('idx-user-role-user_id', 'users_roles', 'user_id');
        $this->createIndex('idx-user-role_id', 'users_roles', 'role_id');

        $this->addForeignKey('fk-user-role-user_id', 'users_roles', 'user_id', 'user', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk-user-role_id', 'users_roles', 'role_id', 'roles', 'id', 'CASCADE', 'CASCADE');


    }

    public function down()
    {
        $this->dropForeignKey('fk-user-role-user_id', 'users_roles');
        $this->dropForeignKey('fk-user-role_id', 'users_roles');
        $this->dropIndex('idx-user-role-user_id', 'users_roles');
        $this->dropIndex('idx-user-role_id', 'users_roles');
        $this->dropTable('users_roles');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

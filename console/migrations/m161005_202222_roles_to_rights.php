<?php

use yii\db\Migration;

class m161005_202222_roles_to_rights extends Migration
{
    public function up()
    {
        $this->createTable('roles_rights', [
            'role_id' => $this->integer()->notNull(),
            'right_id' => $this->integer()->notNull(),
        ]);

        $this->createIndex('idx-role-role_id', 'roles_rights', 'role_id');
        $this->createIndex('idx-role-right_id', 'roles_rights', 'right_id');

        $this->addForeignKey('fk-role-role_id', 'roles_rights', 'role_id', 'roles', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk-role-right_id', 'roles_rights', 'right_id', 'rights', 'id', 'CASCADE', 'CASCADE');

        $this->insert('roles_rights', [
            'role_id'   =>  '1',
            'right_id'  =>  '2',
        ]);
        $this->insert('roles_rights', [
            'role_id'   =>  '1',
            'right_id'  =>  '1',
        ]);
        $this->insert('roles_rights', [
            'role_id'   =>  '2',
            'right_id'  =>  '6',
        ]);
        $this->insert('roles_rights', [
            'role_id'   =>  '2',
            'right_id'  =>  '5',
        ]);
        $this->insert('roles_rights', [
            'role_id'   =>  '3',
            'right_id'  =>  '2',
        ]);
        $this->insert('roles_rights', [
            'role_id'   =>  '3',
            'right_id'  =>  '6',
        ]);
        $this->insert('roles_rights', [
            'role_id'   =>  '3',
            'right_id'  =>  '4',
        ]);
        $this->insert('roles_rights', [
            'role_id'   =>  '3',
            'right_id'  =>  '5',
        ]);
        $this->insert('roles_rights', [
            'role_id'   =>  '3',
            'right_id'  =>  '1',
        ]);
        $this->insert('roles_rights', [
            'role_id'   =>  '3',
            'right_id'  =>  '3',
        ]);
    }

    public function down()
    {
        $this->dropForeignKey('fk-role-role_id', 'roles_rights');
        $this->dropForeignKey('fk-role-right_id', 'roles_rights');
        $this->dropIndex('idx-role-role_id', 'roles_rights');
        $this->dropIndex('idx-role-right_id', 'roles_rights');
        $this->dropTable('roles_rights');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

<?php

use yii\db\Migration;

class m161006_113633_forbidden_rights extends Migration
{
    public function up()
    {
        $this->createTable('forbidden_rights', [
            'user_id' => $this->integer()->notNull(),
            'right_id' => $this->integer()->notNull(),
        ]);

        $this->createIndex('idx-user-forbidden-user_id', 'forbidden_rights', 'user_id');
        $this->createIndex('idx-user-forbidden-right_id', 'forbidden_rights', 'right_id');

        $this->addForeignKey('fk-user-forbidden-user_id', 'forbidden_rights', 'user_id', 'user', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk-user-forbidden-right_id', 'forbidden_rights', 'right_id', 'rights', 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->dropForeignKey('fk-user-forbidden-user_id', 'forbidden_rights');
        $this->dropForeignKey('fk-user-forbidden-right_id', 'forbidden_rights');
        $this->dropIndex('idx-user-forbidden-right_id', 'forbidden_rights');
        $this->dropIndex('idx-user-forbidden-user_id', 'forbidden_rights');
        $this->dropTable('forbidden_rights');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

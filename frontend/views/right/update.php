<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Rigths */

$this->title = 'Update Rigths: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Rigths', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="rigths-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

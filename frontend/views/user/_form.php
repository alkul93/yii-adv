<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;


/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */

$checkedRoles = $model->isNewRecord ? null : ArrayHelper::getColumn($model->roles, 'id');
$checkedRights = $model->isNewRecord ? null : ArrayHelper::getColumn($model->rights, 'id');
$checkedForbidden = $model->isNewRecord ? null : ArrayHelper::getColumn($model->forbiddenRights, 'id');

?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="col-sm-12 col-md-6">
        <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'password')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
    </div>

    <div class="col-sm-12 col-md-6">
        <div class="col-sm-12 form-group">
            <label for="" style="color:blue">Роли пользователя</label>
            <?= Html::checkBoxList('roles', $checkedRoles, $roles); ?>
        </div>
        <div class="col-sm-12 form-group">
            <label for="" style="color:green">Права пользователя</label>
            <?= Html::checkBoxList('rights', $checkedRights, $rights); ?>
        </div>
        <div class="col-sm-12 form-group">
            <label for="" style="color:red">Запреты</label>
            <?= Html::checkBoxList('forbidden', $checkedForbidden, $rights); ?>
        </div>
    </div>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-view">

    <h1><?= Html::encode($model->username) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
    <div class="col-sm-12 col-md-3">
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                'username',
                'email:email',
            ],
        ]) ?>
    </div>

    <div class="col-sm-12 col-md-9">
        <div class="col-sm-12 col-md-3">
            <table class="table">
                <thead>
                    <th>Роли</th>
                </thead>
                <tbody>
                    <?php foreach ($model->roles as $role): ?>
                        <tr class="active">
                            <td><?= $role->name ?></td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
        <div class="col-sm-12 col-md-3">
            <table class="table">
                <thead>
                    <th>Права</th>
                </thead>
                <tbody>
                    <?php foreach ($model->rights as $right): ?>
                        <tr class="warning">
                            <td><?= $right->name ?></td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
        <div class="col-sm-12 col-md-3">
            <table class="table">
                <thead>
                    <th>Запреты</th>
                </thead>
                <tbody>
                    <?php foreach ($model->forbiddenRights as $right): ?>
                        <tr class="danger">
                            <td><?= $right->name ?></td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
        <div class="col-sm-12 col-md-3">
            <table class="table">
                <thead>
                    <th>Результирущие права</th>
                </thead>
                <tbody>
                    <?php foreach ($resultRights as $right): ?>
                        <tr class="success">
                            <td><?= $right->name ?></td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>

</div>

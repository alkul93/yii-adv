<?php

namespace frontend\classes;

use yii;

use console\models\UsersRigths;
use console\models\UsersRoles;
use console\models\ForbiddenRights;
use yii\helpers\Json;




/**
 *
 */
trait UserRelationsTrait
{
    protected function addRoles($model)
    {

        $roles = Yii::$app->request->post('roles');
        if ( isset($roles) && !empty($roles)) {
            foreach ($roles as $val) {
                $userRole = new UsersRoles();
                $userRole->role_id = $val;
                $userRole->user_id = $model->id;

                if (! $userRole->save()) {

                    UsersRoles::deleteAll(["user_id" => $model->id]);
                    return false;
                }

            }
        }

        return true;

    }

    protected function addRights($model)
    {
        $rights = Yii::$app->request->post('rights');
        if (isset($rights) && !empty($rights)) {
            foreach ($rights as $val) {
                $userRights = new UsersRigths();
                $userRights->right_id = $val;
                $userRights->user_id = $model->id;

                if (! $userRights->save()) {

                    UsersRigths::deleteAll(["user_id" => $model->id]);
                    return false;
                }

            }
        }

        return true;


    }

    protected function addForbiddenRights($model)
    {
        $forbiddensRights = Yii::$app->request->post('forbidden');
        if (isset($forbiddensRights) && !empty($forbiddensRights)) {
            foreach ($forbiddensRights as $val) {
                $userForbiddensRight = new ForbiddenRights();
                $userForbiddensRight->right_id = $val;
                $userForbiddensRight->user_id = $model->id;

                if (! $userForbiddensRight->save()) {
                    ForbiddenRights::deleteAll(["user_id" => $model->id]);
                    return false;
                }

            }
        }

        return true;

    }

    protected function clearRelations($model)
    {
        UsersRoles::deleteAll(["user_id" => $model->id]);
        UsersRigths::deleteAll(["user_id" => $model->id]);
        ForbiddenRights::deleteAll(["user_id" => $model->id]);
    }
}

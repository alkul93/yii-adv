<?php

namespace frontend\controllers;

use Yii;
use console\models\Roles;
use console\models\Rigths;
use console\models\RolesRigths;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;


/**
 * RolesController implements the CRUD actions for Roles model.
 */
class RolesController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Roles models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Roles::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Roles model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {

        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Roles model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $error = false;
        // if (Yii::$app->user->isGuest) {
        //     Yii::$app->getSession()->setFlash('error', 'Страница доступна только авторизованым пользователям.');
        //     return $this->redirect('/web/roles');
        // }

        $model = new Roles();
        $rights = ArrayHelper::map(Rigths::find()->all(), 'id', 'name');


        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            // Это конечно можно было бы сделать в afterSave
            // но тогда потеряется слабая связаность в модели, поэтому
            // я считаю это нужно реализовать в контроллере
            $roleRights = Yii::$app->request->post('roleRights');
            foreach ($roleRights as $val) {
                $rolesRight = new RolesRigths();
                $rolesRight->right_id = $val;
                $rolesRight->role_id = $model->id;
                if (! $rolesRight->save()) {
                    $error = true;
                }
            }
            if (!$error) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
            Yii::$app->getSession()->setFlash('error', 'Что то пошло не так');
            return $this->redirect('/web/roles');
        } else {
            return $this->render('create', [
                'model' => $model,
                'rights' => $rights,
            ]);
        }
    }

    /**
     * Updates an existing Roles model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {

        $model = $this->findModel($id);
        $rights = ArrayHelper::map(Rigths::find()->all(), 'id', 'name');


        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            RolesRigths::deleteAll("role_id = $model->id");
            $error = false;

            $roleRights = Yii::$app->request->post('roleRights');

            foreach ($roleRights as $val) {
                $rolesRight = new RolesRigths();
                $rolesRight->right_id = $val;
                $rolesRight->role_id = $model->id;
                if (! $rolesRight->save()) {
                    $error = true;
                }
            }
            if (!$error) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
            Yii::$app->getSession()->setFlash('error', 'Что то пошло не так');
            return $this->redirect('/web/roles');
        } else {
            return $this->render('update', [
                'model' => $model,
                'rights' => $rights,
            ]);
        }
    }

    /**
     * Deletes an existing Roles model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $this->findModel($id)->delete();
        RolesRigths::deleteAll("role_id = $model->id");


        return $this->redirect(['index']);
    }

    /**
     * Finds the Roles model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Roles the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Roles::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

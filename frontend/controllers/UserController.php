<?php

namespace frontend\controllers;

use Yii;
use console\models\User;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

use console\models\Roles;
use console\models\Rigths;
use console\models\UsersRigths;
use console\models\UsersRoles;
use console\models\ForbiddenRights;

use frontend\classes\UserRelationsTrait;


/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller
{
    use UserRelationsTrait;

    public $roles;
    public $rights;
    protected $error = false;

    public function init()
    {
        $this->roles = ArrayHelper::map(Roles::find()->all(), 'id', 'name');
        $this->rights = ArrayHelper::map(Rigths::find()->all(), 'id', 'name');
        // $this->error = false;

    }
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => User::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $roles = ArrayHelper::getColumn($model->roles, 'id');
        $result = array();
        foreach ($model->roles as $role) {
            $ids = ArrayHelper::getColumn($role->rigths, 'id');
            $result = array_merge($result, $ids);
        }

        $ids = ArrayHelper::getColumn($model->rights, 'id');
        $result = array_merge($result, $ids);

        $result = array_unique($result);

        $forbidden = ArrayHelper::getColumn($model->forbiddenRights, 'id');
        $result = array_diff($result, $forbidden);

        $resultRights = Rigths::find()->where(['id' => $result])->all();

        // echo "<pre>";
        // print_r($result);
        // echo "</pre>";
        // exit;

        return $this->render('view', [
            'model'             => $this->findModel($id),
            'resultRights'      => $resultRights,
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new User();

        if ($model->load(Yii::$app->request->post())) {
            $requestUserData = Yii::$app->request->post('User');
            $model->username = $requestUserData['username'];
            $model->email = $requestUserData['email'];
            $model->setPassword($requestUserData['password']);
            $model->generateAuthKey();
            if ($model->save()) {
                /**
                * Аналогично с ролями, можно перенести в afterSave но потеряется
                * слабая связаность, еще рассматривал вариант создать модель,
                * наследумую от модели User, и в ней реализовать сохранение
                * сввязей, но и так, на мой взгляд, достаточно слоев абстракции
                * на мой взгляд лучшим решением является внедрение трейта
                * который будет обрабатывать сохранение в связаные таблицы
                */

                $this->error = $this->addRoles($model) ? false : true;
                $this->error = $this->addRights($model) ? false : true;
                $this->error = $this->addForbiddenRights($model) ? false : true;

            }

            if (! $this->error) {
                return $this->redirect(['view', 'id' => $model->id]);
            }

            Yii::$app->getSession()->setFlash('error', "Что то пошло не так {$this->error}. Обратитесь к програмисту");
            $this->findModel($model->id)->delete();
            return $this->redirect('/web/user');
        } else {
            return $this->render('create', [
                'model'             =>  $model,
                'roles'             =>  $this->roles,
                'rights'            =>  $this->rights,
            ]);
        }
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            // echo "<pre>";
            // print_r(Yii::$app->request->post());
            // echo "</pre>";
            // exit;
            $requestUserData = Yii::$app->request->post('User');
            $model->username = $requestUserData['username'];
            $model->email = $requestUserData['email'];
            if (!empty($requestUserData['password'])) {
                $model->setPassword($requestUserData['password']);
                $model->generateAuthKey();
            }
            if ($model->save()) {
                $this->clearRelations($model);
                $this->error = $this->addRoles($model) ? false : true;
                $this->error = $this->addRights($model) ? false : true;
                $this->error = $this->addForbiddenRights($model) ? false : true;

            }
            if (! $this->error) {
                return $this->redirect(['view', 'id' => $model->id]);
            }

            Yii::$app->getSession()->setFlash('error', 'Что то пошло не так. Обратитесь к програмисту');
            $this->findModel($mode->id)->delete();
            return $this->redirect('/web/user');
        } else {
            return $this->render('update', [
                'model'     =>  $model,
                'roles'     =>  $this->roles,
                'rights'    =>  $this->rights,
            ]);
        }
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
